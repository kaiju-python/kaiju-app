How to use this boilerplate for your new project
------------------------------------------------

Just copy or pull all the boilerplate files to your new repository and set your
new remote.

.. code-block::

    cd my-project
    git init
    git pull git@bitbucket.org:market_app/kaiju-app.git


Replace using your IDE template values by information about your app

.. code-block::

  __APP_NAME__              your application name (will appear in services, logging, cache namespaces etc.)
  __PACKAGE_NAME__          your app name as in pip
  __PACKAGE_VERSION__       your app version as in pip
  __PACKAGE_DESCRIPTION__   your app short description

Note that there are a lot of commented config settings in `config.yml` file and some commented code
in `init_app` and `models/__init__`, `models/tables`. You may uncomment these lines in case you need
certain services (database for example). However you should look at `env.json` settings carefully to
ensure that they are all correct.

Compatibility
-------------

**Python**: 3.8

Summary
-------

- **application.py** - app init function used in `__main__.py`

Docker
------

Docker-compose >=1.28 is supported. There are two docker-compose sample files: the main config file
and the pytest file. Pytest file is expected to be used by app tests and has non-persistent containers.

To run the main file environment and build the app use `full` profile:

.. code-block::

  docker-compose --profile full up

You can customize your environment by adding different profiles to the compose file.

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements. You also will need docker and (probably) docker-compose
to run all tests since they usually require a fresh environment.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv local 3.7.5 3.8.1 3.9.0```

Then you can run `tox` command to test against all of them.

Documentation
-------------

sphinx
^^^^^^

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
