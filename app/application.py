"""
Web app initialization.
"""

from pathlib import Path

from kaiju_tools.services import ServiceContextManager
from kaiju_tools.init_app import init_app as init_app_base

from .models import *
from .routes import urls
from .service_registry import service_class_registry

__all__ = ('init_app',)

DIR = Path(__file__).parent


def init_app(settings):
    """Returns a web app ready to run."""
    attrs = {}
    attrs = {'db_meta': metadata}
    app = init_app_base(settings, attrs=attrs)
    # aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(DIR.parent / 'static' / 'bundle', followlinks=True))
    # app.router.add_static('/static/', path=str(DIR.parent / 'static'))
    router_settings = app.settings['etc']['routes']
    for method, reg, handler, name in urls:
        if reg in router_settings:
            reg = router_settings[reg]
        app.router.add_route(method, reg, handler)
        app.router.add_route(method, reg + "/", handler, name=name)
    return app
