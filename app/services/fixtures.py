import asyncio
import pathlib
from typing import Union

#from kaiju_db.services import DatabaseService
from kaiju_tools.serialization import load
from kaiju_tools.services import ContextableService

__all__ = ['FixtureService']


class FixtureService(ContextableService):
    """This service will init all DB fixtures on app start."""

    service_name = 'fixtures'
    #database_service_class = DatabaseService

    def __init__(
            self, app,
            #database_service: Union[str, database_service_class],
            base_dir: str = './fixtures',
            raise_on_error=False, logger=None):
        """
        :param app:
        :param database_service:
        :param base_dir: root directory for fixtures (relative to the project root)
        :param raise_on_error: raise error if some fixture doesn't exist
        :param logger:
        """

        super().__init__(app=app, logger=logger)
        self._path = pathlib.Path(base_dir).resolve()
        self._path.mkdir(exist_ok=True, parents=True)
        self._raise_on_error = raise_on_error
        #self._db = database_service

    async def init(self):
        """Write your fixture functions here."""

    def _load_fixture(self, name: str):
        """Use this function to import a json file."""

        p = self._path / name
        self.logger.debug('Loading fixture "%s".', p)
        if not p.exists():
            self.logger.warning('Fixture "%s" is not available.', p)
            if self._raise_on_error:
                raise FileNotFoundError(str(p))
        else:
            with open(self._path / name) as f:
                data = load(f)
            return data
